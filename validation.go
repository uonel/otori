package main

import (
	"errors"
	"net/url"
	"regexp"
)

var ( // first needs to match fully, second has to have no match.
	// https://indieauth.spec.indieweb.org/#user-profile-url
	userprofile  = regexp.MustCompile(`https?://(?:\w+\.)*[a-z]*(?:/(?:[^/#\s])+)*/?`)
	userprofile2 = regexp.MustCompile(`/\.+/`)
)
var ( // first needs to match clientid fully, second has to have no match on the url path
	clientid  = regexp.MustCompile(`https?://(?:127\.0\.0\.1|\[::1\]|(?:\w+\.)*[a-z]*)(?::\d+)?(?:/(?:[^/#\s])*)+/?`) // TODO: seems to allow http://u1l.de (without trailing slash)
	clientid2 = regexp.MustCompile(`/\.*/`)
)

func ValidateUserProfile(profile string) error {
	match := userprofile.FindString(profile)
	if len(match) != len(profile) && len(profile) == 0 {
		return errors.New("Invalid Profile URL")
	}
	match2 := userprofile2.FindString(profile)
	if match2 != "" {
		return errors.New("Invalid Profile URL")
	}
	return nil
}

func ValidateClientID(client_id *url.URL) error {
	match := clientid.FindString(client_id.String())
	if len(match) != len(client_id.String()) && len(client_id.String()) == 0 {
		return errors.New("Invalid Client ID")
	}
	match2 := clientid2.FindString(client_id.Path)
	if match2 != "" {
		return errors.New("Invalid Client ID")
	}
	return nil
}

func ValidateClient(raw_client_id string, raw_redirect_uri string) error {
	// check if parameters are empty
	if raw_client_id == "" {
		return errors.New("client_id value missing")
	}
	if raw_redirect_uri == "" {
		return errors.New("redirect_uri value missing")
	}
	// parse client and redirect uri, to be able to compare the host
	client_id, err := url.ParseRequestURI(raw_client_id)
	if err != nil {
		return err
	}
	if verr := ValidateClientID(client_id); verr != nil {
		return verr
	}
	redirect_uri, perr := url.ParseRequestURI(raw_redirect_uri)
	if perr != nil {
		return perr
	}
	if client_id.Host != redirect_uri.Host {
		// search for registered redirect uris, to see if it is valid
		uri_registered := false
		mf, cerr := ParseMicroformats(client_id)
		if cerr != nil {
			return cerr
		}
		if rels, ok := mf.Rels["redirect_uri"]; ok {
			for _, uri := range rels {
				if uri == redirect_uri.String() {
					uri_registered = true
					break
				}
			}
		}
		if !uri_registered {
			return errors.New("redirect_uri is invalid")
		}
	}
	return nil
}
