import sys
import os
import base64
import json
import html
import pickle
from cryptography.fernet import Fernet
import bcrypt
from mf2py import Parser, parse
from jinja2 import Environment, FileSystemLoader, select_autoescape


data = json.loads(sys.argv[1])
env = Environment(
        loader=FileSystemLoader("templates"),
        autoescape=select_autoescape()
)

def readkey():
    if os.path.exists("key.pickle"):
        with open("key.pickle", "rb") as fl:
            return pickle.load(fl)
    else:
        key = Fernet.generate_key()
        with open("key.pickle", "wb") as fl:
            pickle.dump(key, fl)
        return key

def findpwd(mf):
    if "otori_pwd" in mf["rels"]:
        return mf["rel-urls"][mf["rels"]["otori_pwd"][0]]["title"]

def usernameForm(data):
    template = env.get_template("me.html")
    if "name" not in data["client"]:
        data["client"]["name"] = [data["form"]["client_id"][0].replace("https://", "")]
    return template.render(client=data["client"], form=data["form"])

def passwordForm(data):
    template = env.get_template("login.html")
    if "name" not in data["client"]:
        data["client"]["name"] = [data["form"]["client_id"][0].replace("https://", "")]
    me = Parser(url=data["form"]["me"][0]).to_dict(filter_by_type="h-card")[0]["properties"]
    return template.render(client=data["client"], me=me, csrf_snippet = data["csrf"]["snippet"])

def signupForm(data):
    template = env.get_template("signup.html")
    memf = Parser(url=data["form"]["me"][0]).to_dict(filter_by_type="h-card")
    me = {"photo":0, "name":data["form"]["me"]}
    if len(memf) > 0:
        me = memf[0]["properties"]
    return template.render(client=data["client"], me=me, csrf_snippet=data["csrf"]["snippet"])

def signupScreen(data):
    hashed = bcrypt.hashpw(data["form"]["new_password"][0].encode("utf-8"), bcrypt.gensalt())
    f = Fernet(readkey())
    encrypted = f.encrypt(hashed)
    encoded = base64.urlsafe_b64encode(encrypted) 
    me = Parser(url=data["form"]["me"][0]).to_dict(filter_by_type="h-card")[0]["properties"]
    template = env.get_template("signup2.html")
    return template.render(enc_pwd=encoded.decode("utf-8"), me=me, csrf_snippet=data["csrf"]["snippet"])

if "me" not in data["form"]:
    print(usernameForm(data))
    exit()

if "new_password" in data["form"]:
    print(signupScreen(data))
    exit()

if "password" not in data["form"]:
    memf = parse(url=data["form"]["me"][0])
    if not findpwd(memf):
        print(signupForm(data))
        exit()
    print(passwordForm(data))
    exit()


memf = parse(url=data["form"]["me"][0])
encoded = findpwd(memf)
encrypted = base64.urlsafe_b64decode(encoded)
f = Fernet(readkey())
hashed = f.decrypt(encrypted)
if bcrypt.checkpw(data["form"]["password"][0].encode("utf-8"), hashed):
    print(json.dumps({"me": data["form"]["me"][0]}))
    exit(20)
else:
    print(passwordForm(data))
