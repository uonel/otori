package main

import (
	"crypto/rand"
	"encoding/base64"
	"net/http"
	"net/url"
	"time"

	"willnorris.com/go/microformats"
)

func ParseMicroformats(client_id *url.URL) (*microformats.Data, error) {
	if mf, ok := C.Get(client_id.String()); ok {
		return mf.(*microformats.Data), nil
	}

	resp, err := http.Get(client_id.String())
	if err != nil {
		return nil, err
	}
	mf := microformats.Parse(resp.Body, client_id)
	C.Set(client_id.String(), mf, time.Hour)
	return mf, nil
}

func GenerateRandomString(bytes int) string {
	b := make([]byte, bytes)
	rand.Read(b)
	enc := base64.URLEncoding.EncodeToString(b)
	return enc
}
