package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/csrf"
	"github.com/patrickmn/go-cache"
)

var Log = log.New(os.Stderr, "", log.Ldate|log.Ltime|log.Lshortfile)
var C = cache.New(10*time.Minute, 20*time.Minute)

type OAuthError struct {
	Err              string `json:"error"`
	ErrorDescription string `json:"error_description,omitempty"`
	ErrorUri         string `json:"error_uri,omitempty"`
}

func (err OAuthError) Error() []byte {
	rslt, _ := json.Marshal(err)
	return rslt
}

func httpError(f func(w http.ResponseWriter, r *http.Request) (int, error)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		status, err := f(w, r)
		Log.Println(status, err)
		if err != nil {
			w.WriteHeader(400)
			w.Write([]byte(err.Error()))
		}
	}
}

// Reads the Request Body and Ignores nested objects
func parseJsonForm(f func(w http.ResponseWriter, r *http.Request) (int, error)) func(w http.ResponseWriter, r *http.Request) (int, error) {
	return func(w http.ResponseWriter, r *http.Request) (int, error) {
		if strings.Contains(r.Header.Get("Content-Type"), "application/json") {
			form := make(map[string]string)
			data, rerr := ioutil.ReadAll(r.Body)
			if rerr != nil {
				return 400, rerr
			}
			jerr := json.Unmarshal(data, &form)
			if jerr != nil {
				return 400, jerr
			}
			if r.Form == nil {
				r.ParseForm()
			}
			for k, v := range form {
				if _, ok := r.Form[k]; ok {
					r.Form[k] = append(r.Form[k], v)
				} else {
					r.Form[k] = []string{v}
				}
			}
		}
		return f(w, r)
	}
}

func main() {
	fmt.Println("Starting otori...")

	GenerateCodeKey()

	csrfKey := make([]byte, 32)
	_, rerr := rand.Read(csrfKey)
	if rerr != nil {
		log.Println(rerr)
		return
	}
	CSRF := csrf.Protect(csrfKey)

	http.Handle("/login", CSRF(http.HandlerFunc(httpError(parseJsonForm(ShowForm))))) // TODO: make path configurable
	http.HandleFunc("/", httpError(parseJsonForm(HandleIndieAuth)))
	Log.Fatal(http.ListenAndServe(":8085", nil))
}
