module codeberg.org/uonel/otori

go 1.16

require (
	github.com/gorilla/csrf v1.7.1 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	willnorris.com/go/microformats v1.1.1 // indirect
)
