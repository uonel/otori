package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os/exec"
	"syscall"
	"time"

	"github.com/gorilla/csrf"
	"github.com/patrickmn/go-cache"
)

func HandleIndieAuth(w http.ResponseWriter, r *http.Request) (int, error) {
	if err := ValidateClient(r.FormValue("client_id"), r.FormValue("redirect_uri")); err != nil {
		return 400, err
	}
	me := r.FormValue("me")
	if me != "" { // ----------------- && -----↴
		if verr := ValidateUserProfile(me); verr != nil {
			return 400, verr
		}
	}
	if r.Method == "POST" {
		if r.FormValue("code") == "" {
			return 400, errors.New("code value missing")
		}
		return ValidateCode(w, r)
	}
	query := r.URL.RawQuery
	if r.FormValue("flow_id") == "" {
		query += "&flow_id=" + GenerateRandomString(8)
	}
	http.Redirect(w, r, "/login?"+query, http.StatusFound)
	return 0, nil
}

func ShowForm(w http.ResponseWriter, r *http.Request) (int, error) {
	if r.FormValue("flow_id") == "" {
		return 400, errors.New("flow_id value is missing. otori is " +
			"either misconfigured or the authorization endpoint" +
			"link is set wrongly")
	}

	// Build data structure
	data := make(map[string]interface{})
	data["form"] = r.Form
	delete(data["form"].(url.Values), "gorilla.csrf.Token")
	data["request"] = make(map[string]interface{})
	data["request"].(map[string]interface{})["method"] = r.Method
	data["request"].(map[string]interface{})["header"] = r.Header
	data["request"].(map[string]interface{})["address"] = r.RemoteAddr
	data["request"].(map[string]interface{})["url"] = r.URL
	data["request"].(map[string]interface{})["raw_url"] = r.URL.String()

	data["csrf"] = make(map[string]string)
	data["csrf"].(map[string]string)["token"] = csrf.Token(r)
	data["csrf"].(map[string]string)["snippet"] = string(csrf.TemplateField(r))

	client_id, perr := url.ParseRequestURI(r.FormValue("client_id"))
	mf, perr := ParseMicroformats(client_id)
	data["client"] = make(map[string][]interface{})
	if perr == nil {
		for _, itm := range mf.Items {
			is_app := false
			for _, vl := range itm.Type {
				if vl == "h-app" {
					is_app = true
					break
				}
			}
			if is_app {
				data["client"] = itm.Properties
				break
			}
		}
	}

	jsondata, jerr := json.Marshal(data)
	if jerr != nil {
		return 500, jerr
	}
	cmd := exec.Command("python3", "authorization.py", string(jsondata)) // TODO: make this customizable
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	werr := cmd.Run()
	exitcode := 0
	if werr != nil {
		exitcode = werr.(*exec.ExitError).Sys().(syscall.WaitStatus).ExitStatus()
	}
	if exitcode == 20 {
		// Authorization successfull
		return RedirectToApp(w, r, stdout.String())
	}
	log.Println(stderr.String())
	fmt.Fprint(w, stdout.String())
	return 0, nil
}

func RedirectToApp(w http.ResponseWriter, r *http.Request, data string) (int, error) {
	encdata := make(map[string]interface{})
	err := json.Unmarshal([]byte(data), &encdata)
	if err != nil {
		return 500, err
	}
	if verr := ValidateUserProfile(encdata["me"].(string)); verr != nil {
		Log.Println("invalid profile")
		return 500, verr
	}
	codedata := make(map[string]interface{})
	codedata["response"] = encdata
	codedata["client_id"] = r.FormValue("client_id")
	codedata["redirect_uri"] = r.FormValue("redirect_uri")
	codedata["issued"] = time.Now().Unix()
	if c := r.FormValue("code_challenge"); c != "" {
		codedata["code_challenge"] = c
		if r.FormValue("code_challenge_method") == "S256" {
			codedata["code_challenge_method"] = "S256"
		}
	}
	decdata, merr := json.Marshal(codedata)
	if merr != nil {
		return 500, merr
	}

	code := GenerateCode(string(decdata))
	query := "?code=" + code
	if r.FormValue("state") != "" {
		query += "&state=" + url.QueryEscape(r.FormValue("state"))
	}
	http.Redirect(w, r, r.FormValue("redirect_uri")+query, 302)
	return 302, nil
}

func ValidateCode(w http.ResponseWriter, r *http.Request) (int, error) {
	if _, ok := C.Get(r.FormValue("code")[:16]); ok {
		return 401, errors.New("Unauthorized")
	}

	jsondata, err := ReadCode(r.FormValue("code"))
	if err != nil {
		Log.Println("couldn't read code")
		return 401, err
	}
	data := make(map[string]interface{})
	uerr := json.Unmarshal([]byte(jsondata), &data)
	if uerr != nil {
		Log.Println("couldn't decode json")
		return 401, uerr
	}
	duration, _ := time.ParseDuration("10m")
	if time.Since(time.Unix(int64(data["issued"].(float64)), 0)) > duration {
		Log.Println("Code older then 10 minutes")
		return 401, errors.New("Unauthorized")
	}
	if data["client_id"] != r.FormValue("client_id") || data["redirect_uri"] != r.FormValue("redirect_uri") {
		Log.Println("Values missing")
		return 401, errors.New("Unauthorized")
	}
	if _, ok := data["code_challenge"]; ok {
		if r.FormValue("code_verifier") == "" {
			return 401, errors.New("Unauthorized")
		}
		if _, ok := data["code_challenge_method"]; ok {
			h := sha256.New()
			h.Write([]byte(r.FormValue("code_verifier")))
			if base64.RawURLEncoding.EncodeToString(h.Sum(nil)) != data["code_challenge"] {
				return 401, errors.New("Unauthorized")
			}
		} else {
			if data["code_challenge"] != r.FormValue("code_verifier") {
				return 401, errors.New("Unauthorized")
			}
		}
	}

	response, merr := json.Marshal(data["response"])
	if merr != nil {
		Log.Println("couldn't marshal json")
		return 500, merr
	}
	fmt.Fprint(w, string(response))
	C.Add(r.FormValue("code")[:16], true, cache.DefaultExpiration)
	return 200, nil
}
