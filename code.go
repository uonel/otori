package main

import (
	"bytes"
	"compress/zlib"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"io"
	"log"

	"golang.org/x/crypto/nacl/secretbox"
)

var key [32]byte

func GenerateCodeKey() {
	_, err := rand.Read(key[:])
	if err != nil {
		panic(err)
	}
}

func GenerateCode(data string) string {
	var nonce [24]byte
	if _, err := rand.Read(nonce[:]); err != nil {
		log.Println(err)
		panic(err)
	}
	encrypted := secretbox.Seal(nonce[:], []byte(data), &nonce, &key)
	zipped := bytes.NewBuffer(nil)
	writer, err := zlib.NewWriterLevel(zipped, 9)
	if err != nil {
		log.Println(err)
		panic(err)
	}
	_, err = writer.Write(encrypted)
	if err != nil {
		log.Println(err)
		panic(err)
	}
	writer.Close()
	return base64.URLEncoding.EncodeToString(zipped.Bytes())
}

func ReadCode(code string) (string, error) {
	zipped, err := base64.URLEncoding.DecodeString(code)
	if err != nil {
		return "", err
	}
	reader, err := zlib.NewReader(bytes.NewBuffer(zipped))
	if err != nil {
		return "", err
	}
	encrypted, err := io.ReadAll(reader)
	if err != nil {
		return "", err
	}
	if len(encrypted) <= 24 {
		return "", errors.New("code to short")
	}
	var nonce [24]byte
	copy(nonce[:], encrypted[:24])
	decrypted, ok := secretbox.Open(nil, encrypted[24:], &nonce, &key)
	if !ok {
		return string(decrypted), errors.New("Decryption not ok")
	}
	return string(decrypted), nil
}
